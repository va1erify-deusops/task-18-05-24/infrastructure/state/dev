# Network
output "network_id" {
  value = module.network.id
}

output "network_name" {
  value = module.network.name
}

# Subnetwork
output "subnetwork_id" {
  value = module.subnetwork.id
}

output "subnetwork_network_id" {
  value = module.subnetwork.network_id
}

output "subnetwork_v4_cidr_blocks" {
  value = module.subnetwork.v4_cidr_blocks
}

output "subnetwork_zone" {
  value = module.subnetwork.zone
}

# External ip
output "ingress_external_ipv4_adress" {
  value = module.ingress_external_ip.external_ipv4_address
}

# Security group
output "security_group_id" {
  value = module.security_group.id
}

output "security_group_name" {
  value = module.security_group.name
}

output "security_group_network_id" {
  value = module.security_group.network_id
}

output "security_group_folder_id" {
  value = module.security_group.folder_id
}

# k8s service account
output "k8_service_account_name" {
  value = module.k8_service_account.name
}

output "k8_service_account_id" {
  value = module.k8_service_account.id
}

output "k8_service_account_folder_id" {
  value = module.k8_service_account.folder_id
}

# folder_iam_binding_admin
output "folder_iam_binding_admin_id" {
  value = module.folder_iam_binding_admin.id
}

output "folder_iam_binding_admin_folder_id" {
  value = module.folder_iam_binding_admin.folder_id
}

output "folder_iam_binding_admin_role" {
  value = module.folder_iam_binding_admin.role
}

output "folder_iam_binding_admin_members" {
  value = module.folder_iam_binding_admin.members
}

#folder_iam_binding_editor
output "folder_iam_binding_editor_id" {
  value = module.folder_iam_binding_editor.id
}

output "folder_iam_binding_editor_folder_id" {
  value = module.folder_iam_binding_editor.folder_id
}

output "folder_iam_binding_editor_members" {
  value = module.folder_iam_binding_editor.members
}

output "folder_iam_binding_editor_role" {
  value = module.folder_iam_binding_editor.role
}

# folder_iam_member_cluster_agent_role_for_SA
output "folder_iam_member_cluster_agent_role_for_SA_id" {
  value = module.folder_iam_member_cluster_agent_role_for_SA.id
}

output "folder_iam_member_cluster_agent_role_for_SA_role" {
  value = module.folder_iam_member_cluster_agent_role_for_SA.role
}

output "folder_iam_member_cluster_agent_role_for_SA_member" {
  value = module.folder_iam_member_cluster_agent_role_for_SA.member
}

output "folder_iam_member_cluster_agent_role_for_SA_folder_id" {
  value = module.folder_iam_member_cluster_agent_role_for_SA.folder_id
}

# folder_iam_member_container_registry_images_puller_role_for_SA
output "folder_iam_member_container_registry_images_puller_role_for_SA_id" {
  value = module.folder_iam_member_container_registry_images_puller_role_for_SA.id
}

output "folder_iam_member_container_registry_images_puller_role_for_SA_folder_id" {
  value = module.folder_iam_member_container_registry_images_puller_role_for_SA.folder_id
}

output "folder_iam_member_container_registry_images_puller_role_for_SA_member" {
  value = module.folder_iam_member_container_registry_images_puller_role_for_SA.member
}

output "folder_iam_member_container_registry_images_puller_role_for_SA_role" {
  value = module.folder_iam_member_container_registry_images_puller_role_for_SA.role
}

# folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA
output "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA_role" {
  value = module.folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA.role
}

output "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA_member" {
  value = module.folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA.member
}

output "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA_folder_id" {
  value = module.folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA.folder_id
}

output "folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA_id" {
  value = module.folder_iam_member_kms_keys_encrypter_decrypter_role_for_SA.id
}

# folder_iam_member_vpc_public_admin_role_for_SA
output "folder_iam_member_vpc_public_admin_role_for_SA_id" {
  value = module.folder_iam_member_vpc_public_admin_role_for_SA.id
}

output "folder_iam_member_vpc_public_admin_role_for_SA_folder_id" {
  value = module.folder_iam_member_vpc_public_admin_role_for_SA.folder_id
}

output "folder_iam_member_vpc_public_admin_role_for_SA_member" {
  value = module.folder_iam_member_vpc_public_admin_role_for_SA.member
}

output "folder_iam_member_vpc_public_admin_role_for_SA_role" {
  value = module.folder_iam_member_vpc_public_admin_role_for_SA.role
}


# K8s cluster
output "k8s_cluster_name" {
  value = module.kubernetes_cluster.name
}

output "k8s_cluster_id" {
  value = module.kubernetes_cluster.id
}

output "k8s_cluster_ipv4_range" {
  value = module.kubernetes_cluster.cluster_ipv4_range
}

output "k8s_cluster_folder_id" {
  value = module.kubernetes_cluster.folder_id
}

output "k8s_cluster_network_id" {
  value = module.kubernetes_cluster.network_id
}

output "kubernetes_node_group_name" {
  value = module.kubernetes_node_group.name
}

output "kubernetes_node_group_scale_policy" {
  value = module.kubernetes_node_group.scale_policy
}

output "kubernetes_node_group_status" {
  value = module.kubernetes_node_group.status
}

output "kubernetes_node_group_version" {
  value = module.kubernetes_node_group.version
}

output "kubernetes_node_group_created_at" {
  value = module.kubernetes_node_group.created_at
}

output "kubernetes_node_group_maintenance_policy" {
  value = module.kubernetes_node_group.maintenance_policy
}

output "kubernetes_node_group_labels" {
  value = module.kubernetes_node_group.labels
}

output "kubernetes_node_group_instance_template" {
  value = module.kubernetes_node_group.instance_template
}

output "kubernetes_node_group_instance_group_id" {
  value = module.kubernetes_node_group.instance_group_id
}

output "kubernetes_node_group_cluster_id" {
  value = module.kubernetes_node_group.cluster_id
}